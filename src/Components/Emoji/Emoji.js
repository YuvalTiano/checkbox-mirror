import React from 'react';
import PropTypes from 'prop-types';
import './Emoji.css';

class Emoji extends React.PureComponent {
    static propTypes = {
        activeEmoji: PropTypes.string,
        unActiveEmoji: PropTypes.string,
    };

    static defaultProps = {
        activeEmoji: `💩`,
        unActiveEmoji: `👻`,
    };
    
    render() {
        const {
            checked,
            width,
            activeEmoji,
            unActiveEmoji,
        } = this.props;

        const style = {
            flexBasis: `${width}%`,
        }

        return (
            <span role="img" aria-label="emoji" style={ style }>
                {
                    checked
                        ? `${activeEmoji}`
                        : `${unActiveEmoji}`
                }
            </span>
        )
    }

}

export default Emoji;
