import React from 'react';
import CheckBox from '../CheckBox';
import VideoController from '../VideoController';
import isMobileDevice from '../../Utils/isMobileDevice';
import './Video.css';

class Video extends React.Component {
    constructor(props){
        super(props);

        this.videoElement = React.createRef();
        this.canvasElement = React.createRef();
        this.isMobileDevice = isMobileDevice();

        this.state = {
            frame: Array(80*60).fill(255),
            width: this.isMobileDevice ? 40 : 80,
            height: this.isMobileDevice ? 30 : 60,
            brightness: 127,
        }
    }

    askForMediaDevicesPermission = () => {
        console.log(this.videoElement)
        const OPTIONS = {
            video: {
                width: this.state.width,
                height: this.state.height,
            },
            audio: false,
        };

        navigator.mediaDevices.getUserMedia(OPTIONS)
            .then(stream => {
                console.log(stream);
                if(stream.active){
                    this.videoElement.current.srcObject = stream;
                    this.videoElement.current.onloadedmetadata = (event) => this.videoElement.current.play();
                }
                return stream;
            })
            .catch(err => {
                console.error(err);
            });
    }

    componentDidMount() {
        this.askForMediaDevicesPermission();
        setInterval(this.drawImage, 1000 / 24);
    }

    drawImage = () => {
        this.canvasElement.current.getContext('2d').drawImage(this.videoElement.current, 0, 0, this.state.width, this.state.height);
        const capturedStream = this.canvasElement.current.getContext('2d').getImageData(0, 0, this.state.width, this.state.height);
        const frame = [];

        for (let i = 0; i < capturedStream.data.length; i += 4 ) {
            const pixel = parseInt((capturedStream.data[i] + capturedStream.data[i+1] + capturedStream.data[i+2]) / 3);
            frame.push(pixel);
        }

        this.setState({frame})
    }

    updateState = (newState) => {
        this.setState(newState);
    }
    
    render() {
        const {
            frame,
            width,
            height,
            brightness,
        } = this.state;
        const gridWidth = 12.8 * width;
        const checkboxWidth = 100 / width;

        return (
            <React.Fragment>
                <video ref={ this.videoElement } width={ width } height={ height } controls />
                <canvas ref={ this.canvasElement } width={ width } height={ height } />
                <VideoController
                    width={ width }
                    height={ height }
                    brightness={ brightness }
                    changeGridProperties={ this.updateState }
                    drawImage={ this.drawImage }
                />
                <div className="frame" style={{ width: gridWidth }}>
                    {
                        frame.map((checkboxBrightness, index) => (
                            <CheckBox
                                key={index}
                                checked={ checkboxBrightness <= brightness }
                                width={ checkboxWidth }
                            />
                        ))
                    }
                </div>
            </React.Fragment>
        )
    }
}

export default Video;
