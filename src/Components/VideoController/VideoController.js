import React from 'react';
import './VideoController.style.css'

class VideoController extends React.PureComponent {
    calculateContainerSize = (event) => {
        switch (event.target.name) {
            case 'width':
                this.props.changeGridProperties({
                    width: parseInt(event.target.value),
                    height: parseInt(event.target.value * 3 / 4),
                });
                break;
            case 'height':
                this.props.changeGridProperties({
                    width: parseInt(event.target.value * 4 / 3),
                    height: parseInt(event.target.value),
                });
                break;
            default:
                break;
        }
    }

    setBrightness = (event) => {
        this.props.changeGridProperties({brightness: parseInt(event.target.value)});
    }

    render() {
        const {
            width,
            height,
            brightness,
        } = this.props;

        return (
            <div className="video-controller-form">
                <input name="width" value={ width } type="number" min="0" max="320" onChange={ this.calculateContainerSize } />
                <input name="height" value={ height } type="number" min="0" max="240" onChange={ this.calculateContainerSize } />
                <input type="range" min="0" max="255" defaultValue={ brightness } onChange={ this.setBrightness } />
                <button onClick={() => this.props.drawImage()}>drawImage</button>
            </div>
        );
    }
}

export default VideoController;
