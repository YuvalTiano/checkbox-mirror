import React from 'react';
import PropType from 'prop-types';

class CheckBox extends React.PureComponent {
    static propTypes = {
        checked: PropType.bool.isRequired,
        width: PropType.number.isRequired,
    }
    
    render() {
        const {
            checked,
            width,
        } = this.props;

        const style = {
            flexBasis: `${width}%`,
        }

        return <input type='checkbox' checked={ checked } style={ style } readOnly />
    }
}

export default CheckBox;
